package com.henderson_group.core.domain.enumeration;

/**
 * The CommodityStatus enumeration.
 */
public enum CommodityStatus {
    ACTIVE,INACTIVE,DELETED
}
