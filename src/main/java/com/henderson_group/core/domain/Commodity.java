package com.henderson_group.core.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

import com.henderson_group.core.domain.enumeration.CommodityStatus;

/**
 * A Commodity.
 */
@Entity
@Table(name = "commodity")
public class Commodity implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "cm_code")
    private Integer cmCode;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "status")
    private CommodityStatus status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getCmCode() {
        return cmCode;
    }

    public Commodity cmCode(Integer cmCode) {
        this.cmCode = cmCode;
        return this;
    }

    public void setCmCode(Integer cmCode) {
        this.cmCode = cmCode;
    }

    public String getDescription() {
        return description;
    }

    public Commodity description(String description) {
        this.description = description;
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public CommodityStatus getStatus() {
        return status;
    }

    public Commodity status(CommodityStatus status) {
        this.status = status;
        return this;
    }

    public void setStatus(CommodityStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Commodity commodity = (Commodity) o;
        if (commodity.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, commodity.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Commodity{" +
            "id=" + id +
            ", cmCode='" + cmCode + "'" +
            ", description='" + description + "'" +
            ", status='" + status + "'" +
            '}';
    }
}
