package com.henderson_group.core.domain;


import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

/**
 * A Address.
 */
@Entity
@Table(name = "address")
public class Address implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    private Long id;

    @Column(name = "house_no")
    private String houseNo;

    @Column(name = "street")
    private String street;

    @Column(name = "city")
    private String city;

    @Column(name = "county_or_province")
    private String countyOrProvince;

    @Column(name = "zip_or_post_code")
    private String zipOrPostCode;

    @Column(name = "country")
    private String country;

    @Column(name = "other_details")
    private String otherDetails;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getHouseNo() {
        return houseNo;
    }

    public Address houseNo(String houseNo) {
        this.houseNo = houseNo;
        return this;
    }

    public void setHouseNo(String houseNo) {
        this.houseNo = houseNo;
    }

    public String getStreet() {
        return street;
    }

    public Address street(String street) {
        this.street = street;
        return this;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public Address city(String city) {
        this.city = city;
        return this;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getCountyOrProvince() {
        return countyOrProvince;
    }

    public Address countyOrProvince(String countyOrProvince) {
        this.countyOrProvince = countyOrProvince;
        return this;
    }

    public void setCountyOrProvince(String countyOrProvince) {
        this.countyOrProvince = countyOrProvince;
    }

    public String getZipOrPostCode() {
        return zipOrPostCode;
    }

    public Address zipOrPostCode(String zipOrPostCode) {
        this.zipOrPostCode = zipOrPostCode;
        return this;
    }

    public void setZipOrPostCode(String zipOrPostCode) {
        this.zipOrPostCode = zipOrPostCode;
    }

    public String getCountry() {
        return country;
    }

    public Address country(String country) {
        this.country = country;
        return this;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getOtherDetails() {
        return otherDetails;
    }

    public Address otherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
        return this;
    }

    public void setOtherDetails(String otherDetails) {
        this.otherDetails = otherDetails;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Address address = (Address) o;
        if (address.id == null || id == null) {
            return false;
        }
        return Objects.equals(id, address.id);
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(id);
    }

    @Override
    public String toString() {
        return "Address{" +
            "id=" + id +
            ", houseNo='" + houseNo + "'" +
            ", street='" + street + "'" +
            ", city='" + city + "'" +
            ", countyOrProvince='" + countyOrProvince + "'" +
            ", zipOrPostCode='" + zipOrPostCode + "'" +
            ", country='" + country + "'" +
            ", otherDetails='" + otherDetails + "'" +
            '}';
    }
}
