package com.henderson_group.core.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.henderson_group.core.domain.Retailer;

import com.henderson_group.core.repository.RetailerRepository;
import com.henderson_group.core.web.rest.util.HeaderUtil;
import com.henderson_group.core.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Retailer.
 */
@RestController
@RequestMapping("/api")
public class RetailerResource {

    private final Logger log = LoggerFactory.getLogger(RetailerResource.class);

    private static final String ENTITY_NAME = "retailer";
        
    private final RetailerRepository retailerRepository;

    public RetailerResource(RetailerRepository retailerRepository) {
        this.retailerRepository = retailerRepository;
    }

    /**
     * POST  /retailers : Create a new retailer.
     *
     * @param retailer the retailer to create
     * @return the ResponseEntity with status 201 (Created) and with body the new retailer, or with status 400 (Bad Request) if the retailer has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/retailers")
    @Timed
    public ResponseEntity<Retailer> createRetailer(@Valid @RequestBody Retailer retailer) throws URISyntaxException {
        log.debug("REST request to save Retailer : {}", retailer);
        if (retailer.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new retailer cannot already have an ID")).body(null);
        }
        Retailer result = retailerRepository.save(retailer);
        return ResponseEntity.created(new URI("/api/retailers/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /retailers : Updates an existing retailer.
     *
     * @param retailer the retailer to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated retailer,
     * or with status 400 (Bad Request) if the retailer is not valid,
     * or with status 500 (Internal Server Error) if the retailer couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/retailers")
    @Timed
    public ResponseEntity<Retailer> updateRetailer(@Valid @RequestBody Retailer retailer) throws URISyntaxException {
        log.debug("REST request to update Retailer : {}", retailer);
        if (retailer.getId() == null) {
            return createRetailer(retailer);
        }
        Retailer result = retailerRepository.save(retailer);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, retailer.getId().toString()))
            .body(result);
    }

    /**
     * GET  /retailers : get all the retailers.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of retailers in body
     */
    @GetMapping("/retailers")
    @Timed
    public ResponseEntity<List<Retailer>> getAllRetailers(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Retailers");
        Page<Retailer> page = retailerRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/retailers");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /retailers/:id : get the "id" retailer.
     *
     * @param id the id of the retailer to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the retailer, or with status 404 (Not Found)
     */
    @GetMapping("/retailers/{id}")
    @Timed
    public ResponseEntity<Retailer> getRetailer(@PathVariable Long id) {
        log.debug("REST request to get Retailer : {}", id);
        Retailer retailer = retailerRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(retailer));
    }

    /**
     * DELETE  /retailers/:id : delete the "id" retailer.
     *
     * @param id the id of the retailer to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/retailers/{id}")
    @Timed
    public ResponseEntity<Void> deleteRetailer(@PathVariable Long id) {
        log.debug("REST request to delete Retailer : {}", id);
        retailerRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
