package com.henderson_group.core.web.rest;

import com.codahale.metrics.annotation.Timed;
import com.henderson_group.core.domain.Commodity;

import com.henderson_group.core.repository.CommodityRepository;
import com.henderson_group.core.web.rest.util.HeaderUtil;
import com.henderson_group.core.web.rest.util.PaginationUtil;
import io.swagger.annotations.ApiParam;
import io.github.jhipster.web.util.ResponseUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Optional;

/**
 * REST controller for managing Commodity.
 */
@RestController
@RequestMapping("/api")
public class CommodityResource {

    private final Logger log = LoggerFactory.getLogger(CommodityResource.class);

    private static final String ENTITY_NAME = "commodity";
        
    private final CommodityRepository commodityRepository;

    public CommodityResource(CommodityRepository commodityRepository) {
        this.commodityRepository = commodityRepository;
    }

    /**
     * POST  /commodities : Create a new commodity.
     *
     * @param commodity the commodity to create
     * @return the ResponseEntity with status 201 (Created) and with body the new commodity, or with status 400 (Bad Request) if the commodity has already an ID
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PostMapping("/commodities")
    @Timed
    public ResponseEntity<Commodity> createCommodity(@RequestBody Commodity commodity) throws URISyntaxException {
        log.debug("REST request to save Commodity : {}", commodity);
        if (commodity.getId() != null) {
            return ResponseEntity.badRequest().headers(HeaderUtil.createFailureAlert(ENTITY_NAME, "idexists", "A new commodity cannot already have an ID")).body(null);
        }
        Commodity result = commodityRepository.save(commodity);
        return ResponseEntity.created(new URI("/api/commodities/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * PUT  /commodities : Updates an existing commodity.
     *
     * @param commodity the commodity to update
     * @return the ResponseEntity with status 200 (OK) and with body the updated commodity,
     * or with status 400 (Bad Request) if the commodity is not valid,
     * or with status 500 (Internal Server Error) if the commodity couldnt be updated
     * @throws URISyntaxException if the Location URI syntax is incorrect
     */
    @PutMapping("/commodities")
    @Timed
    public ResponseEntity<Commodity> updateCommodity(@RequestBody Commodity commodity) throws URISyntaxException {
        log.debug("REST request to update Commodity : {}", commodity);
        if (commodity.getId() == null) {
            return createCommodity(commodity);
        }
        Commodity result = commodityRepository.save(commodity);
        return ResponseEntity.ok()
            .headers(HeaderUtil.createEntityUpdateAlert(ENTITY_NAME, commodity.getId().toString()))
            .body(result);
    }

    /**
     * GET  /commodities : get all the commodities.
     *
     * @param pageable the pagination information
     * @return the ResponseEntity with status 200 (OK) and the list of commodities in body
     */
    @GetMapping("/commodities")
    @Timed
    public ResponseEntity<List<Commodity>> getAllCommodities(@ApiParam Pageable pageable) {
        log.debug("REST request to get a page of Commodities");
        Page<Commodity> page = commodityRepository.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(page, "/api/commodities");
        return new ResponseEntity<>(page.getContent(), headers, HttpStatus.OK);
    }

    /**
     * GET  /commodities/:id : get the "id" commodity.
     *
     * @param id the id of the commodity to retrieve
     * @return the ResponseEntity with status 200 (OK) and with body the commodity, or with status 404 (Not Found)
     */
    @GetMapping("/commodities/{id}")
    @Timed
    public ResponseEntity<Commodity> getCommodity(@PathVariable Long id) {
        log.debug("REST request to get Commodity : {}", id);
        Commodity commodity = commodityRepository.findOne(id);
        return ResponseUtil.wrapOrNotFound(Optional.ofNullable(commodity));
    }

    /**
     * DELETE  /commodities/:id : delete the "id" commodity.
     *
     * @param id the id of the commodity to delete
     * @return the ResponseEntity with status 200 (OK)
     */
    @DeleteMapping("/commodities/{id}")
    @Timed
    public ResponseEntity<Void> deleteCommodity(@PathVariable Long id) {
        log.debug("REST request to delete Commodity : {}", id);
        commodityRepository.delete(id);
        return ResponseEntity.ok().headers(HeaderUtil.createEntityDeletionAlert(ENTITY_NAME, id.toString())).build();
    }

}
