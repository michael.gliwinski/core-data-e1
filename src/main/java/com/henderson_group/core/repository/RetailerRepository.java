package com.henderson_group.core.repository;

import com.henderson_group.core.domain.Retailer;

import org.springframework.data.jpa.repository.*;

import java.util.List;

/**
 * Spring Data JPA repository for the Retailer entity.
 */
@SuppressWarnings("unused")
public interface RetailerRepository extends JpaRepository<Retailer,Long> {

}
