import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { RouterModule } from '@angular/router';

import { CoreDataE1SharedModule } from '../../shared';

import {
    CommodityService,
    CommodityPopupService,
    CommodityComponent,
    CommodityDetailComponent,
    CommodityDialogComponent,
    CommodityPopupComponent,
    CommodityDeletePopupComponent,
    CommodityDeleteDialogComponent,
    commodityRoute,
    commodityPopupRoute,
    CommodityResolvePagingParams,
} from './';

let ENTITY_STATES = [
    ...commodityRoute,
    ...commodityPopupRoute,
];

@NgModule({
    imports: [
        CoreDataE1SharedModule,
        RouterModule.forRoot(ENTITY_STATES, { useHash: true })
    ],
    declarations: [
        CommodityComponent,
        CommodityDetailComponent,
        CommodityDialogComponent,
        CommodityDeleteDialogComponent,
        CommodityPopupComponent,
        CommodityDeletePopupComponent,
    ],
    entryComponents: [
        CommodityComponent,
        CommodityDialogComponent,
        CommodityPopupComponent,
        CommodityDeleteDialogComponent,
        CommodityDeletePopupComponent,
    ],
    providers: [
        CommodityService,
        CommodityPopupService,
        CommodityResolvePagingParams,
    ],
    schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreDataE1CommodityModule {}
