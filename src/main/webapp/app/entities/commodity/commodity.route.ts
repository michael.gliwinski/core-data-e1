import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot, Routes, CanActivate } from '@angular/router';

import { UserRouteAccessService } from '../../shared';
import { PaginationUtil } from 'ng-jhipster';

import { CommodityComponent } from './commodity.component';
import { CommodityDetailComponent } from './commodity-detail.component';
import { CommodityPopupComponent } from './commodity-dialog.component';
import { CommodityDeletePopupComponent } from './commodity-delete-dialog.component';

import { Principal } from '../../shared';

@Injectable()
export class CommodityResolvePagingParams implements Resolve<any> {

  constructor(private paginationUtil: PaginationUtil) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
      let page = route.queryParams['page'] ? route.queryParams['page'] : '1';
      let sort = route.queryParams['sort'] ? route.queryParams['sort'] : 'id,asc';
      return {
          page: this.paginationUtil.parsePage(page),
          predicate: this.paginationUtil.parsePredicate(sort),
          ascending: this.paginationUtil.parseAscending(sort)
    };
  }
}

export const commodityRoute: Routes = [
  {
    path: 'commodity',
    component: CommodityComponent,
    resolve: {
      'pagingParams': CommodityResolvePagingParams
    },
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.commodity.home.title'
    }
  }, {
    path: 'commodity/:id',
    component: CommodityDetailComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.commodity.home.title'
    }
  }
];

export const commodityPopupRoute: Routes = [
  {
    path: 'commodity-new',
    component: CommodityPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.commodity.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'commodity/:id/edit',
    component: CommodityPopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.commodity.home.title'
    },
    outlet: 'popup'
  },
  {
    path: 'commodity/:id/delete',
    component: CommodityDeletePopupComponent,
    data: {
        authorities: ['ROLE_USER'],
        pageTitle: 'coreDataE1App.commodity.home.title'
    },
    outlet: 'popup'
  }
];
