
const enum CommodityStatus {
    'ACTIVE',
    'INACTIVE',
    'DELETED'

};
export class Commodity {
    constructor(
        public id?: number,
        public cmCode?: number,
        public description?: string,
        public status?: CommodityStatus,
    ) {
    }
}
