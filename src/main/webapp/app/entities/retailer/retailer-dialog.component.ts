import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Response } from '@angular/http';

import { NgbActiveModal, NgbModalRef } from '@ng-bootstrap/ng-bootstrap';
import { EventManager, AlertService, JhiLanguageService } from 'ng-jhipster';

import { Retailer } from './retailer.model';
import { RetailerPopupService } from './retailer-popup.service';
import { RetailerService } from './retailer.service';
import { Address, AddressService } from '../address';

@Component({
    selector: 'jhi-retailer-dialog',
    templateUrl: './retailer-dialog.component.html'
})
export class RetailerDialogComponent implements OnInit {

    retailer: Retailer;
    authorities: any[];
    isSaving: boolean;

    addresses: Address[];
    constructor(
        public activeModal: NgbActiveModal,
        private jhiLanguageService: JhiLanguageService,
        private alertService: AlertService,
        private retailerService: RetailerService,
        private addressService: AddressService,
        private eventManager: EventManager
    ) {
        this.jhiLanguageService.setLocations(['retailer']);
    }

    ngOnInit() {
        this.isSaving = false;
        this.authorities = ['ROLE_USER', 'ROLE_ADMIN'];
        this.addressService.query().subscribe(
            (res: Response) => { this.addresses = res.json(); }, (res: Response) => this.onError(res.json()));
    }
    clear () {
        this.activeModal.dismiss('cancel');
    }

    save () {
        this.isSaving = true;
        if (this.retailer.id !== undefined) {
            this.retailerService.update(this.retailer)
                .subscribe((res: Retailer) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        } else {
            this.retailerService.create(this.retailer)
                .subscribe((res: Retailer) =>
                    this.onSaveSuccess(res), (res: Response) => this.onSaveError(res.json()));
        }
    }

    private onSaveSuccess (result: Retailer) {
        this.eventManager.broadcast({ name: 'retailerListModification', content: 'OK'});
        this.isSaving = false;
        this.activeModal.dismiss(result);
    }

    private onSaveError (error) {
        this.isSaving = false;
        this.onError(error);
    }

    private onError (error) {
        this.alertService.error(error.message, null, null);
    }

    trackAddressById(index: number, item: Address) {
        return item.id;
    }
}

@Component({
    selector: 'jhi-retailer-popup',
    template: ''
})
export class RetailerPopupComponent implements OnInit, OnDestroy {

    modalRef: NgbModalRef;
    routeSub: any;

    constructor (
        private route: ActivatedRoute,
        private retailerPopupService: RetailerPopupService
    ) {}

    ngOnInit() {
        this.routeSub = this.route.params.subscribe(params => {
            if ( params['id'] ) {
                this.modalRef = this.retailerPopupService
                    .open(RetailerDialogComponent, params['id']);
            } else {
                this.modalRef = this.retailerPopupService
                    .open(RetailerDialogComponent);
            }

        });
    }

    ngOnDestroy() {
        this.routeSub.unsubscribe();
    }
}
