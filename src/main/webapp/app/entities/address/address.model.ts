export class Address {
    constructor(
        public id?: number,
        public houseNo?: string,
        public street?: string,
        public city?: string,
        public countyOrProvince?: string,
        public zipOrPostCode?: string,
        public country?: string,
        public otherDetails?: string,
    ) {
    }
}
