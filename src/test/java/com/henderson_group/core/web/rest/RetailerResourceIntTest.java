package com.henderson_group.core.web.rest;

import com.henderson_group.core.CoreDataE1App;

import com.henderson_group.core.domain.Retailer;
import com.henderson_group.core.repository.RetailerRepository;
import com.henderson_group.core.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * Test class for the RetailerResource REST controller.
 *
 * @see RetailerResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreDataE1App.class)
public class RetailerResourceIntTest {

    private static final Integer DEFAULT_CODE = 10000;
    private static final Integer UPDATED_CODE = 10001;

    private static final String DEFAULT_NAME = "AAAAAAAAAA";
    private static final String UPDATED_NAME = "BBBBBBBBBB";

    @Autowired
    private RetailerRepository retailerRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restRetailerMockMvc;

    private Retailer retailer;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        RetailerResource retailerResource = new RetailerResource(retailerRepository);
        this.restRetailerMockMvc = MockMvcBuilders.standaloneSetup(retailerResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Retailer createEntity(EntityManager em) {
        Retailer retailer = new Retailer()
            .code(DEFAULT_CODE)
            .name(DEFAULT_NAME);
        return retailer;
    }

    @Before
    public void initTest() {
        retailer = createEntity(em);
    }

    @Test
    @Transactional
    public void createRetailer() throws Exception {
        int databaseSizeBeforeCreate = retailerRepository.findAll().size();

        // Create the Retailer
        restRetailerMockMvc.perform(post("/api/retailers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(retailer)))
            .andExpect(status().isCreated());

        // Validate the Retailer in the database
        List<Retailer> retailerList = retailerRepository.findAll();
        assertThat(retailerList).hasSize(databaseSizeBeforeCreate + 1);
        Retailer testRetailer = retailerList.get(retailerList.size() - 1);
        assertThat(testRetailer.getCode()).isEqualTo(DEFAULT_CODE);
        assertThat(testRetailer.getName()).isEqualTo(DEFAULT_NAME);
    }

    @Test
    @Transactional
    public void createRetailerWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = retailerRepository.findAll().size();

        // Create the Retailer with an existing ID
        retailer.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restRetailerMockMvc.perform(post("/api/retailers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(retailer)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Retailer> retailerList = retailerRepository.findAll();
        assertThat(retailerList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void checkCodeIsRequired() throws Exception {
        int databaseSizeBeforeTest = retailerRepository.findAll().size();
        // set the field null
        retailer.setCode(null);

        // Create the Retailer, which fails.

        restRetailerMockMvc.perform(post("/api/retailers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(retailer)))
            .andExpect(status().isBadRequest());

        List<Retailer> retailerList = retailerRepository.findAll();
        assertThat(retailerList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    public void getAllRetailers() throws Exception {
        // Initialize the database
        retailerRepository.saveAndFlush(retailer);

        // Get all the retailerList
        restRetailerMockMvc.perform(get("/api/retailers?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(retailer.getId().intValue())))
            .andExpect(jsonPath("$.[*].code").value(hasItem(DEFAULT_CODE)))
            .andExpect(jsonPath("$.[*].name").value(hasItem(DEFAULT_NAME.toString())));
    }

    @Test
    @Transactional
    public void getRetailer() throws Exception {
        // Initialize the database
        retailerRepository.saveAndFlush(retailer);

        // Get the retailer
        restRetailerMockMvc.perform(get("/api/retailers/{id}", retailer.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(retailer.getId().intValue()))
            .andExpect(jsonPath("$.code").value(DEFAULT_CODE))
            .andExpect(jsonPath("$.name").value(DEFAULT_NAME.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingRetailer() throws Exception {
        // Get the retailer
        restRetailerMockMvc.perform(get("/api/retailers/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateRetailer() throws Exception {
        // Initialize the database
        retailerRepository.saveAndFlush(retailer);
        int databaseSizeBeforeUpdate = retailerRepository.findAll().size();

        // Update the retailer
        Retailer updatedRetailer = retailerRepository.findOne(retailer.getId());
        updatedRetailer
            .code(UPDATED_CODE)
            .name(UPDATED_NAME);

        restRetailerMockMvc.perform(put("/api/retailers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedRetailer)))
            .andExpect(status().isOk());

        // Validate the Retailer in the database
        List<Retailer> retailerList = retailerRepository.findAll();
        assertThat(retailerList).hasSize(databaseSizeBeforeUpdate);
        Retailer testRetailer = retailerList.get(retailerList.size() - 1);
        assertThat(testRetailer.getCode()).isEqualTo(UPDATED_CODE);
        assertThat(testRetailer.getName()).isEqualTo(UPDATED_NAME);
    }

    @Test
    @Transactional
    public void updateNonExistingRetailer() throws Exception {
        int databaseSizeBeforeUpdate = retailerRepository.findAll().size();

        // Create the Retailer

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restRetailerMockMvc.perform(put("/api/retailers")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(retailer)))
            .andExpect(status().isCreated());

        // Validate the Retailer in the database
        List<Retailer> retailerList = retailerRepository.findAll();
        assertThat(retailerList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteRetailer() throws Exception {
        // Initialize the database
        retailerRepository.saveAndFlush(retailer);
        int databaseSizeBeforeDelete = retailerRepository.findAll().size();

        // Get the retailer
        restRetailerMockMvc.perform(delete("/api/retailers/{id}", retailer.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Retailer> retailerList = retailerRepository.findAll();
        assertThat(retailerList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Retailer.class);
    }
}
