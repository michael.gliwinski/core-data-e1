package com.henderson_group.core.web.rest;

import com.henderson_group.core.CoreDataE1App;

import com.henderson_group.core.domain.Commodity;
import com.henderson_group.core.repository.CommodityRepository;
import com.henderson_group.core.web.rest.errors.ExceptionTranslator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import com.henderson_group.core.domain.enumeration.CommodityStatus;
/**
 * Test class for the CommodityResource REST controller.
 *
 * @see CommodityResource
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = CoreDataE1App.class)
public class CommodityResourceIntTest {

    private static final Integer DEFAULT_CM_CODE = 1;
    private static final Integer UPDATED_CM_CODE = 2;

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final CommodityStatus DEFAULT_STATUS = CommodityStatus.ACTIVE;
    private static final CommodityStatus UPDATED_STATUS = CommodityStatus.INACTIVE;

    @Autowired
    private CommodityRepository commodityRepository;

    @Autowired
    private MappingJackson2HttpMessageConverter jacksonMessageConverter;

    @Autowired
    private PageableHandlerMethodArgumentResolver pageableArgumentResolver;

    @Autowired
    private ExceptionTranslator exceptionTranslator;

    @Autowired
    private EntityManager em;

    private MockMvc restCommodityMockMvc;

    private Commodity commodity;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
        CommodityResource commodityResource = new CommodityResource(commodityRepository);
        this.restCommodityMockMvc = MockMvcBuilders.standaloneSetup(commodityResource)
            .setCustomArgumentResolvers(pageableArgumentResolver)
            .setControllerAdvice(exceptionTranslator)
            .setMessageConverters(jacksonMessageConverter).build();
    }

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Commodity createEntity(EntityManager em) {
        Commodity commodity = new Commodity()
            .cmCode(DEFAULT_CM_CODE)
            .description(DEFAULT_DESCRIPTION)
            .status(DEFAULT_STATUS);
        return commodity;
    }

    @Before
    public void initTest() {
        commodity = createEntity(em);
    }

    @Test
    @Transactional
    public void createCommodity() throws Exception {
        int databaseSizeBeforeCreate = commodityRepository.findAll().size();

        // Create the Commodity
        restCommodityMockMvc.perform(post("/api/commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commodity)))
            .andExpect(status().isCreated());

        // Validate the Commodity in the database
        List<Commodity> commodityList = commodityRepository.findAll();
        assertThat(commodityList).hasSize(databaseSizeBeforeCreate + 1);
        Commodity testCommodity = commodityList.get(commodityList.size() - 1);
        assertThat(testCommodity.getCmCode()).isEqualTo(DEFAULT_CM_CODE);
        assertThat(testCommodity.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testCommodity.getStatus()).isEqualTo(DEFAULT_STATUS);
    }

    @Test
    @Transactional
    public void createCommodityWithExistingId() throws Exception {
        int databaseSizeBeforeCreate = commodityRepository.findAll().size();

        // Create the Commodity with an existing ID
        commodity.setId(1L);

        // An entity with an existing ID cannot be created, so this API call must fail
        restCommodityMockMvc.perform(post("/api/commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commodity)))
            .andExpect(status().isBadRequest());

        // Validate the Alice in the database
        List<Commodity> commodityList = commodityRepository.findAll();
        assertThat(commodityList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    public void getAllCommodities() throws Exception {
        // Initialize the database
        commodityRepository.saveAndFlush(commodity);

        // Get all the commodityList
        restCommodityMockMvc.perform(get("/api/commodities?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(commodity.getId().intValue())))
            .andExpect(jsonPath("$.[*].cmCode").value(hasItem(DEFAULT_CM_CODE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION.toString())))
            .andExpect(jsonPath("$.[*].status").value(hasItem(DEFAULT_STATUS.toString())));
    }

    @Test
    @Transactional
    public void getCommodity() throws Exception {
        // Initialize the database
        commodityRepository.saveAndFlush(commodity);

        // Get the commodity
        restCommodityMockMvc.perform(get("/api/commodities/{id}", commodity.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
            .andExpect(jsonPath("$.id").value(commodity.getId().intValue()))
            .andExpect(jsonPath("$.cmCode").value(DEFAULT_CM_CODE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION.toString()))
            .andExpect(jsonPath("$.status").value(DEFAULT_STATUS.toString()));
    }

    @Test
    @Transactional
    public void getNonExistingCommodity() throws Exception {
        // Get the commodity
        restCommodityMockMvc.perform(get("/api/commodities/{id}", Long.MAX_VALUE))
            .andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    public void updateCommodity() throws Exception {
        // Initialize the database
        commodityRepository.saveAndFlush(commodity);
        int databaseSizeBeforeUpdate = commodityRepository.findAll().size();

        // Update the commodity
        Commodity updatedCommodity = commodityRepository.findOne(commodity.getId());
        updatedCommodity
            .cmCode(UPDATED_CM_CODE)
            .description(UPDATED_DESCRIPTION)
            .status(UPDATED_STATUS);

        restCommodityMockMvc.perform(put("/api/commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(updatedCommodity)))
            .andExpect(status().isOk());

        // Validate the Commodity in the database
        List<Commodity> commodityList = commodityRepository.findAll();
        assertThat(commodityList).hasSize(databaseSizeBeforeUpdate);
        Commodity testCommodity = commodityList.get(commodityList.size() - 1);
        assertThat(testCommodity.getCmCode()).isEqualTo(UPDATED_CM_CODE);
        assertThat(testCommodity.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testCommodity.getStatus()).isEqualTo(UPDATED_STATUS);
    }

    @Test
    @Transactional
    public void updateNonExistingCommodity() throws Exception {
        int databaseSizeBeforeUpdate = commodityRepository.findAll().size();

        // Create the Commodity

        // If the entity doesn't have an ID, it will be created instead of just being updated
        restCommodityMockMvc.perform(put("/api/commodities")
            .contentType(TestUtil.APPLICATION_JSON_UTF8)
            .content(TestUtil.convertObjectToJsonBytes(commodity)))
            .andExpect(status().isCreated());

        // Validate the Commodity in the database
        List<Commodity> commodityList = commodityRepository.findAll();
        assertThat(commodityList).hasSize(databaseSizeBeforeUpdate + 1);
    }

    @Test
    @Transactional
    public void deleteCommodity() throws Exception {
        // Initialize the database
        commodityRepository.saveAndFlush(commodity);
        int databaseSizeBeforeDelete = commodityRepository.findAll().size();

        // Get the commodity
        restCommodityMockMvc.perform(delete("/api/commodities/{id}", commodity.getId())
            .accept(TestUtil.APPLICATION_JSON_UTF8))
            .andExpect(status().isOk());

        // Validate the database is empty
        List<Commodity> commodityList = commodityRepository.findAll();
        assertThat(commodityList).hasSize(databaseSizeBeforeDelete - 1);
    }

    @Test
    @Transactional
    public void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Commodity.class);
    }
}
