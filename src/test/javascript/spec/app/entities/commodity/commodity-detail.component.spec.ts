import { ComponentFixture, TestBed, async, inject } from '@angular/core/testing';
import { MockBackend } from '@angular/http/testing';
import { Http, BaseRequestOptions } from '@angular/http';
import { OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs/Rx';
import { DateUtils, DataUtils } from 'ng-jhipster';
import { JhiLanguageService } from 'ng-jhipster';
import { MockLanguageService } from '../../../helpers/mock-language.service';
import { MockActivatedRoute } from '../../../helpers/mock-route.service';
import { CommodityDetailComponent } from '../../../../../../main/webapp/app/entities/commodity/commodity-detail.component';
import { CommodityService } from '../../../../../../main/webapp/app/entities/commodity/commodity.service';
import { Commodity } from '../../../../../../main/webapp/app/entities/commodity/commodity.model';

describe('Component Tests', () => {

    describe('Commodity Management Detail Component', () => {
        let comp: CommodityDetailComponent;
        let fixture: ComponentFixture<CommodityDetailComponent>;
        let service: CommodityService;

        beforeEach(async(() => {
            TestBed.configureTestingModule({
                declarations: [CommodityDetailComponent],
                providers: [
                    MockBackend,
                    BaseRequestOptions,
                    DateUtils,
                    DataUtils,
                    DatePipe,
                    {
                        provide: ActivatedRoute,
                        useValue: new MockActivatedRoute({id: 123})
                    },
                    {
                        provide: Http,
                        useFactory: (backendInstance: MockBackend, defaultOptions: BaseRequestOptions) => {
                            return new Http(backendInstance, defaultOptions);
                        },
                        deps: [MockBackend, BaseRequestOptions]
                    },
                    {
                        provide: JhiLanguageService,
                        useClass: MockLanguageService
                    },
                    CommodityService
                ]
            }).overrideComponent(CommodityDetailComponent, {
                set: {
                    template: ''
                }
            }).compileComponents();
        }));

        beforeEach(() => {
            fixture = TestBed.createComponent(CommodityDetailComponent);
            comp = fixture.componentInstance;
            service = fixture.debugElement.injector.get(CommodityService);
        });


        describe('OnInit', () => {
            it('Should call load all on init', () => {
            // GIVEN

            spyOn(service, 'find').and.returnValue(Observable.of(new Commodity(10)));

            // WHEN
            comp.ngOnInit();

            // THEN
            expect(service.find).toHaveBeenCalledWith(123);
            expect(comp.commodity).toEqual(jasmine.objectContaining({id:10}));
            });
        });
    });

});
